from django.db import models
from django.urls import reverse

# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=120, verbose_name='Başlık')
    text = models.TextField(verbose_name='Metin')
    video = models.FileField(null=False, blank=False, verbose_name='Video')
    date = models.DateTimeField(verbose_name="Tarih", auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post:detail', kwargs={'id': self.id})
